import matplotlib
matplotlib.use('Agg')

import config
from cancernet import CancerNet

from tensorflow.keras.utils import to_categorical
import tensorflow as tf
from tensorflow.keras.optimizers import Adagrad
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.data import AUTOTUNE

import os
import argparse
from imutils import paths
import time


def load_image(imagePath):
    image = tf.io.read_file(imagePath)
    image = tf.image.decode_png(image, channels=3)
    image = tf.image.convert_image_dtype(image, dtype=tf.float32)
    image = tf.image.resize(image, config.IMAGE_SIZE)

    label = tf.strings.split(imagePath, os.path.sep)[-2]
    label = tf.strings.to_number(label, tf.int32)

    return image, label


@tf.function
def augmentation(image, label):
    # perform random left right and up down image flip
    image = tf.image.flip_left_right(image)
    image = tf.image.flip_up_down(image)

    # return the image and label
    return image, label


ap = argparse.ArgumentParser()
ap.add_argument('-p', '--plot', type=str, default='plot.png', help='path to save the image')
args = vars(ap.parse_args())

# print(args['plot'])

trainPaths = list(paths.list_images(config.TRAIN_PATH))
valPaths = list(paths.list_images(config.VAL_PATH))
testPaths = list(paths.list_images(config.TEST_PATH))

trainLabels = [int(trainPath.split(os.path.sep)[-2]) for trainPath in trainPaths]
trainLabels = to_categorical(trainLabels)
classTotals = trainLabels.sum(axis=0)
classWeights = {}

for i in range(0, len(classTotals)):
    classWeights[i] = classTotals.max() / classTotals[i]

trainDS = tf.data.Dataset.from_tensor_slices(trainPaths)
trainDS = (trainDS
           .shuffle(len(trainPaths))
           .map(load_image, num_parallel_calls=AUTOTUNE)
           .map(augmentation, num_parallel_calls=AUTOTUNE)
           .cache()
           .batch(config.BS)
           .prefetch(AUTOTUNE))


valDS = tf.data.Dataset.from_tensor_slices(valPaths)
valDS = (valDS
         .map(load_image, num_parallel_calls=AUTOTUNE)
         .cache()
         .batch(config.BS)
         .prefetch(AUTOTUNE))


testDS = tf.data.Dataset.from_tensor_slices(testPaths)
testDS = (testDS
          .map(load_image, num_parallel_calls=AUTOTUNE)
          .cache()
          .batch(config.BS)
          .prefetch(AUTOTUNE))


model = CancerNet.build(height=config.IMAGE_SIZE[0], width=config.IMAGE_SIZE[1], depth=3, classes=1)
opt = Adagrad(lr=config.INIT_LR, decay=config.INIT_LR / config.NUM_EPOCHS)
model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
es = EarlyStopping(patience=config.EARLY_STOPPING_PATIENCE, restore_best_weights=True)

start = time.time()
H = model.fit(x=trainDS,
              validation_data=valDS,
              class_weight=classWeights,
              epochs=config.NUM_EPOCHS,
              callbacks=[es],
              verbose=1)
end = time.time()

print('[TF data] training time for {} epochs is: {:.2f} seconds'.format(config.NUM_EPOCHS, (end-start)))
