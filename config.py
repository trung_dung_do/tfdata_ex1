import os
from imutils import paths
import numpy as np
import cv2

# initialize the path to the original input directory of images
ORIG_INPUT_DATASET = '/Users/macone/Documents/3_dataset/IDC/orig'

# initialize the path to the new directory that will contain images after
# computing the training and testing split
BASE_PATH = 'split'

TRAIN_PATH = os.path.sep.join([BASE_PATH, 'training'])
VAL_PATH = os.path.sep.join([BASE_PATH, 'validation'])
TEST_PATH = os.path.sep.join([BASE_PATH, 'testing'])

# define the amount of data that will be used training
TRAIN_SPLIT = 0.8

# the amount of validation data will be a percentage of the training data
VAL_SPLIT = 0.1

# define the input image spatial dimensions
IMAGE_SIZE = (48, 48)

NUM_EPOCHS = 40
EARLY_STOPPING_PATIENCE = 5
INIT_LR = 1e-2
BS = 128


