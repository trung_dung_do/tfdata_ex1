import matplotlib
matplotlib.use('Agg')

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import LearningRateScheduler
from tensorflow.keras.optimizers import Adagrad
from tensorflow.keras.utils import to_categorical
from sklearn.metrics import classification_report, confusion_matrix
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os

from cancernet import CancerNet
import config
import time


ap = argparse.ArgumentParser()
ap.add_argument('-p', '--plot', type=str, default='plot.png', help='path to accuracy / loss plot')
args = vars(ap.parse_args())

print(args['plot'])


trainPaths = list(paths.list_images(config.TRAIN_PATH))
totalTrain = len(trainPaths)
totalVal = len(list(paths.list_images(config.VAL_PATH)))
totalTest = len(list(paths.list_images(config.TEST_PATH)))

trainLabels = [int(p.split(os.path.sep)[-2]) for p in trainPaths]
trainLabels = to_categorical(trainLabels)
print(len(trainPaths))
classTotals = trainLabels.sum(axis=0)
classWeight = dict()

for i in range(0, len(classTotals)):
    classWeight[i] = classTotals.max() / classTotals[i]

trainAug = ImageDataGenerator(
    rescale=1/255.,
    rotation_range=20,
    zoom_range=0.05,
    width_shift_range=0.1,
    height_shift_range=0.1,
    shear_range=0.05,
    horizontal_flip=True,
    vertical_flip=True,
    fill_mode='nearest'
)

valAug = ImageDataGenerator(rescale=1./255.)

trainGen = trainAug.flow_from_directory(
    config.TRAIN_PATH,
    class_mode='categorical',
    target_size=config.IMAGE_SIZE,
    color_mode='rgb',
    shuffle=True,
    batch_size=config.BS
)

valGen = valAug.flow_from_directory(
    config.VAL_PATH,
    class_mode='categorical',
    target_size=config.IMAGE_SIZE,
    color_mode='rgb',
    shuffle=False,
    batch_size=config.BS
)

testGen = valAug.flow_from_directory(
    config.TEST_PATH,
    class_mode='categorical',
    target_size=config.IMAGE_SIZE,
    color_mode='rgb',
    shuffle=False,
    batch_size=config.BS
)

model = CancerNet.build(width=config.IMAGE_SIZE[0],
                        height=config.IMAGE_SIZE[1],
                        depth=3,
                        classes=2)
opt = Adagrad(lr=config.INIT_LR, decay=config.INIT_LR / config.NUM_EPOCHS)
model.compile(loss='binary_crossentropy',
              optimizer=opt,
              metrics=['accuracy'])

start = time.time()
H = model.fit(x=trainGen,
              steps_per_epoch=totalTrain // config.BS,
              validation_data=valGen,
              validation_steps=totalVal // config.BS,
              class_weight=classWeight,
              epochs=config.NUM_EPOCHS)
end = time.time()

print('[Image Data Generator] training time for {} epochs is: {:.2f} seconds'.format(config.NUM_EPOCHS, (end-start)))

print('[INFO] evaluating network...')
testGen.reset()
predIdxs = model.predict(x=testGen, steps=(totalTest//config.BS)+1)
predIdxs = np.argmax(predIdxs, axis=1)
print(classification_report(testGen.classes, predIdxs,
                            target_names=testGen.class_indices.keys()))

# compute the confusion matrix and use it to derive the raw
# accuracy, sensitivity and specificity
cm = confusion_matrix(testGen.classes, predIdxs)
total = sum(sum(cm))

acc = (cm[0, 0] + cm[1, 1]) / total
sensitivity = cm[0, 0] / (cm[0, 0] + cm[0, 1])
specificity = cm[1, 1] / (cm[1, 1] + cm[1, 0])

print(cm)
print('acc: {:.4f}'.format(acc))
print('sensitivity: {:.4f}'.format(sensitivity))
print('specificity: {:.4f}'.format(specificity))


# plot the training loss and accuracy
N = config.NUM_EPOCHS
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, N), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, N), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, N), H.history["accuracy"], label="train_acc")
plt.plot(np.arange(0, N), H.history["val_accuracy"], label="val_acc")
plt.title("Training Loss and Accuracy on Dataset")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="lower left")
plt.savefig(args["plot"])

print('Done!')


